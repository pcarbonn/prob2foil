import time
import logging

from problog.program import PrologFile

from probfoil import probfoil
from probfoil.data import DataFile
from probfoil.score import accuracy, precision, recall

logger = logging.getLogger("probfoil")
logger.setLevel(logging.INFO)
logging.info("probfoil")
time_start = time.time()

##############
data = DataFile(*(PrologFile('c:\\Python\\BitBucket\\prob2foil\\examples\\family.'+ source) for source in ('settings', 'data')))
learn = probfoil.ProbFOIL2(data)
hypothesis = learn.learn()
##############

time_total = time.time() - time_start
print ('Total time:\t%.4fs' % time_total)

if learn.interrupted:
    print('================ PARTIAL THEORY ================')
else:
    print('================= FINAL THEORY =================')
rules = hypothesis.to_clauses(hypothesis.target.functor)

# First rule is failing rule: don't print it if there are other rules.
if len(rules) > 1:
    for rule in rules[1:]:
        print (rule)
else:
    print (rules[0])
print ('==================== SCORES ====================')
print ('            Accuracy:\t', accuracy(hypothesis))
print ('           Precision:\t', precision(hypothesis))
print ('              Recall:\t', recall(hypothesis))
print ('================== STATISTICS ==================')
for name, value in learn.statistics():
    print ('%20s:\t%s' % (name, value))
